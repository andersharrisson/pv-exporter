import click
import os
import re
import threading
import time
import yaml
from p4p.client.thread import Context
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))

pva_context = Context("pva")


class PVCollector(object):
    def __init__(self, iocs):
        self.iocs = iocs

    def collect(self):
        lock = threading.Lock()

        pv_metric = GaugeMetricFamily("pv_value", "Value of EPICS PV", labels=["pv"])

        threads = []
        for ioc in self.iocs:
            thread = threading.Thread(
                target=self.get_pvs, args=(ioc["exports"], 1, pv_metric, lock)
            )
            threads.append(thread)
            thread.start()
        for thread in threads:
            thread.join()
        yield pv_metric

    def get_pvs(self, pvs, timeout, pv_metric, lock):
        try:
            values = pva_context.get(pvs, timeout=timeout)
        except TimeoutError:
            print("Timeout while trying to get pvs: ", pvs)
            return
        lock.acquire()
        for i in range(len(pvs)):
            pv_metric.add_metric([pvs[i]], values[i])
        lock.release()


@click.command()
@click.option("--port", default=12111, help="TCP port the exporter will listen on")
@click.option(
    "--config",
    default=os.path.join(SCRIPT_DIR, "pv-exporter.yaml"),
    type=click.Path(exists=True),
    help="Configuration file",
)
def main(port, config):
    with open(config, "r") as config_file:
        try:
            iocs = yaml.safe_load(config_file)
        except yaml.YAMLError as error:
            print(error)
            return
    REGISTRY.register(PVCollector(iocs))
    start_http_server(port)
    while True:
        time.sleep(1)


if __name__ == "__main__":
    main()
