FROM python

RUN pip install prometheus_client p4p click pyYAML

RUN useradd -ms /bin/bash pv-exporter
USER pv-exporter

COPY --chown=pv-exporter:pv-exporter pv-exporter.* /home/pv-exporter/
WORKDIR /home/pv-exporter
CMD python pv-exporter.py
